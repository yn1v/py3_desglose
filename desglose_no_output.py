#!/usr/bin/python3
# -*- coding: utf-8 -*-

denominacion = [500, 200, 100, 50, 20, 10, 5, 1, 0.5, 0.25, 0.10, 0.05, 0.01]
billetes = 10  # valor del billete de menor denominacion

planilla_file = open("sample.csv", "r")
planilla_list = planilla_file.readlines()
planilla_file.close()

desglose = {}

for i in planilla_list:
    empleado = i.strip()
    empleado = empleado.split(",")
    empleado_actual = empleado[0]
    if empleado_actual not in desglose:
        desglose[empleado_actual] = {}
    monto_pagar = empleado[1]
    desglose[empleado_actual]["a pagar"] = float(monto_pagar)

# print(desglose)

for empleado in list(desglose.keys()):
    print("Empleado", empleado)
    pagarc = desglose[empleado]["a pagar"] * 100
    print("A pagar", pagarc)
    for d in denominacion:
        dc = d*100
        print("d=", dc)
        if pagarc > dc:
            print("pagar > d")
            cantidad = int(pagarc // dc)
            print("cantidad", cantidad)
            pagarc -= int(cantidad*dc)
            print("pagar", pagarc)
        elif pagarc == dc:
            print("pagar == d")
            cantidad = 1
            print("cantidad", cantidad)
            pagarc -= int(cantidad * dc)
            print("pagar", pagarc)
        else:
            print("pagar < d")
            cantidad = 0
            print("cantidad", cantidad)
            print("pagar", pagarc)
        dc = int(d/100)
        desglose[empleado][d] = int(cantidad)
        print("empleado", empleado, "d", d, "cantidad", cantidad)
print(desglose)

resumen = {}

for empleado in list(desglose.keys()):
    for d in denominacion:
        if d not in resumen:
            resumen[d] = 0
        resumen[d] += desglose[empleado][d]

# print(resumen)

print ("Resumen del desglose")
total = 0
for d in denominacion:
    sub_total = resumen[d] * d
    if d >= billetes:
        print(resumen[d], "Billetes de", d, "=", round(sub_total,2))
    elif d >= 1:
        print(resumen[d], "Monedas de", d, "=", round(sub_total,2))
    else:
        print(resumen[d], "Monedas de", int(d * 100), "=", round(sub_total,2))
    total += sub_total
print("Total =", round(total,2))
# output = open("output.csv", "w")
print("Formato de Sobres,,,,,,,,,,,,,,")
print(("Empleado"), end=",")
for d in denominacion:
    print(d, end= ",")
print("")
for empleado in list(desglose.keys()):
    print((empleado), end=',')
    for d in denominacion:
        print(desglose[empleado][d], end = ",")
    print("")
# output.close()
