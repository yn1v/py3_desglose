#!/usr/bin/python3
# -*- coding: utf-8 -*-

from decimal import *
# getcontext().prec = 2

denominaciones = ['500','200', '100', '50', '20','10', '5', '1','0.5', '0.25', '0.10', '0.05', '0.01']
billetes = Decimal('10')  # valor del billete de menor denominacion

denominacion =[]
for d in denominaciones:
    d = Decimal(d)
    denominacion.append(d)
print(denominacion)

planilla_file = open("abonos.csv", "r")
planilla_list = planilla_file.readlines()
planilla_file.close()

desglose = {}

for i in planilla_list:
    empleado = i.strip()
    empleado = empleado.split(",")
    empleado_actual = empleado[0]
    if empleado_actual not in desglose:
        desglose[empleado_actual] = {}
    monto_pagar = Decimal(empleado[1])
    desglose[empleado_actual]["a pagar"] = monto_pagar

# print(desglose)

for empleado in list(desglose.keys()):
    pagar = desglose[empleado]["a pagar"]
    print("pagar:", type(pagar), pagar)
    for d in denominacion:
        print("d", type(d), d)
        if pagar > d:
            cantidad = int(pagar // d)
            # pagar -= cantidad * Decimal(d)
            pagar = Decimal(pagar % d)
        elif pagar == d:
            cantidad = 1
            pagar -= pagar
        else:
            cantidad = 0
        desglose[empleado][d] = int(cantidad)

print(desglose)

resumen = {}

for empleado in list(desglose.keys()):
    for d in denominacion:
        if d not in resumen:
            resumen[d] = 0
        resumen[d] += desglose[empleado][d]

# print(resumen)

print ("Resumen del desglose")
total = 0
for d in denominacion:
    sub_total = Decimal(resumen[d] * d)
    if d >= billetes:
        print(resumen[d], "Billetes de", d, "=", sub_total)
    elif d >= 1:
        print(resumen[d], "Monedas de", d, "=", sub_total)
    else:
        print(resumen[d], "Monedas de", int(d * 100), "=", sub_total)
    total += sub_total
print("Total =", total)


output = open("output_abonos.csv", "w")
print("Formato de Sobres,,,,,,,,,,,,,,", file=output)
print(("Empleado, Por Pagar"), end=",", file=output)
for d in denominacion:
    print(d, end= ",", file=output)
print("", file=output)
for empleado in list(desglose.keys()):
    print((empleado), end=',', file=output)
    print((desglose[empleado]["a pagar"]), end=',', file=output)
    for d in denominacion:
        print(desglose[empleado][d], end = ",", file=output)
    print("", file=output)
output.close()
